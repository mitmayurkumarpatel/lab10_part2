package calculator;

/**
 *
 * @author mitpa
 */
public class Substract extends Calculator 
{
    
    public Substract(double x, double y){
    super(x,y);
}
    
    public double substract() {
        return x - y;
    }
    
}
