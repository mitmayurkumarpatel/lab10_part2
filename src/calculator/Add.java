package calculator;

/**
 *
 * @author mitpa
 */
public class Add extends Calculator 
{
    
    public Add(double x, double y){
    super(x,y);
}
    
    public double add() {
        return x + y;
    }
    
}
