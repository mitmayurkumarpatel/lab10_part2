package calculator;
/**
 *
 * @author mitpa
 */
public class Multiply extends Calculator {
    
    public Multiply( double x, double y)
    {
        super(x , y);
    }
    
     public double multiply() 
     {
        return x * y;
    }
}
